<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/commune/{commune}', 'CommuneController@index')->name('commune');
//ruta para el index
Route::get('/schools', 'SchoolsController@index');
//ruta para mostrar colegio
Route::get('/schools/rbd/{rbd}', 'SchoolsController@getSchool');
//ruta para enviar variable 
Route::post('/{q}', 'SchoolsController@index');
//ruta para el autocomplete
Route::get('schools/{q}', 'SchoolsController@autocomplete');

//ruta para el index de las comunas
Route::get('/commune', 'CommuneController@index');
//ruta para enviar variable 
//Route::post('/schools/{commune}', 'CommuneController@index');

Route::get('commune/autocomplete/{commune}', 'CommuneController@autocomplete_commune');