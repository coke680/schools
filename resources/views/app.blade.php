<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Colegios</title>
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.2.0/dist/leaflet.css"/>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- Latest compiled and minified CSS & JS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <script src="//code.jquery.com/jquery.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://bootswatch.com/flatly/bootstrap.css">
        <link rel="stylesheet" href="/css/custom.css">
        <!-- JS file -->
        <script src="/js/jquery.easy-autocomplete.js"></script>
        <!-- CSS file -->
        <link rel="stylesheet" href="/css/easy-autocomplete.css">
</head>
<div id="preloader">
  <div id="status">&nbsp;</div>
</div>
<body>    
    <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">Colegios</a>
            </div>
    
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div>
    </nav>

	<div class="container" style="min-height: 600px;">
        <div class="row">
            <div class="col-lg-12">
                @yield('content')
            </div>
        </div>                    
    </div>
        <div id="footer">
            <div class="container">
              <div class="row text-center">
                
                  <div class="col-sm-3">
                    <div class="foot-header">
                      Acerca de Nosotros <img src="http://200.27.156.170/ean_default/img/cocha/icon-cocha.png">
                    </div>
                    <div class="foot-links">
                      <a href="http://internet.cocha.com/nuestra-empresa/nuestra-empresa.html">Nuestra Empresa</a>
                      <a href="http://cms.cocha.com/sucursales.html">Sucursales</a>
                      <a href="http://internet.cocha.com/_DisenoWeb/minisitios/sitio/por-que-comprar-en-cocha.html?cid=por-que-comprar-en-cocha">Por qué comprar en Cocha</a>
                      <a href="http://internet.cocha.com/virgin_galactic/virgin_galactic.html">Virgin Galactic</a>
                      <a href="http://internet.cocha.com/_DisenoWeb/minisitios/sitio/trabaja-con-nosotros.html?cid=trabaja_en_cocha">Trabaja con nosotros</a>
                      <a href="http://www.cochainbound.com/" target="_blank">¿Vienes a Chile?</a>
                    </div>
                  </div><!--/col-sm-3-->
                <div class="col-sm-3">
                  <div class="foot-header"> Servicio al cliente <img src="http://200.27.156.170/ean_default/img/cocha/servicio-al-cliente-icon.png"></div>
                  <div class="foot-links">
                    <a href="javascript:Contacto()">Contáctanos</a>
                    <a href="http://internet.cocha.com/_DisenoWeb/minisitios/sitio/servicio-al-pasajero.html?cid=atencion-al-pasajero">Atención al pasajero</a>
                    <a href="http://internet.cocha.com/_DisenoWeb/minisitios/sitio/faq.html?cid=preguntas-frecuentes">Preguntas frecuentes</a>
                    <a href="http://cms.cocha.com/terminos-y-condiciones">Términos y Condiciones Generales</a>
                    <a href="http://internet.cocha.com/_DisenoWeb/check-in/check-in.html?cid=check-in-on-line">Check-in en línea </a>
                    <a href="http://www.cocha.com/ibe/bookingManagement/retrieveBookingForm.do">Consultar reserva</a>
                    <a href="http://internet.cocha.com/manual-del-viajero.pdf" target="_blank">Manual del viajero</a>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="foot-header">
                    Medios de pago <img src="http://200.27.156.170/ean_default/img/cocha/card-icon.png">
                  </div>
                  <div class="foot-links">
                    <p>
                      <i class="fa fa-check text-success"></i> Tarjetas de crédito<br>
                      <img src="http://200.27.156.170/ean_default/img/cocha/visa-card.png">
                      <img src="http://200.27.156.170/ean_default/img/cocha/master-card.png">
                      <img src="http://200.27.156.170/ean_default/img/cocha/dinner-club-card.png">
                    </p>
                    <p>
                      <i class="fa fa-check text-success"></i> Transferencias bancarias<br>
                      <img src="http://200.27.156.170/ean_default/img/cocha/banco-santander-card-1.png">
                      <img src="http://200.27.156.170/ean_default/img/cocha/banco-de-chile-card-1.png">
                    </p>
                    <p>
                      <i class="fa fa-check text-success"></i> Tarjeta Ripley<br>
                      <img src="http://200.27.156.170/ean_default/img/cocha/ripley-card.png">
                    </p>
                  </div>
                </div><!--/col-sm-3-->
                <div class="col-sm-3">
                  <img src="http://200.27.156.170/ean_default/img/cocha/tripadvisor-logo.jpg" class="img-responsive img-thumbnail">
                  <br><br>
                  <a href="http://internet.cocha.com/especiales/sello-de-calidad-turistica.html" style="float: center;">
                    <img src="http://200.27.156.170/ean_default/img/cocha/sello_de_calidad_turistica.png" width="121" height="70" alt="sello de calidad turistica">
                  </a>
                  <a href="http://www.chileestuyo.cl/" target="_blanck" style="float: left; margin-left: 20px;">
                    <img src="http://200.27.156.170/ean_default/img/cocha/Logo_Chileestuyo.png" height="70" alt="Logo Chileestuyo">
                  </a>
                </div>                
            </div><!--/row-->
        </div><!--/footer--></div>

        <!-- Modal search-->
        <div class="modal fade" id="modal-search">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Busca tu colegio</h4>
                    </div>
                    <div class="modal-body">
                        <div role="tabpanel">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#home" aria-controls="home" role="tab" data-toggle="tab">Nombre o RBD</a>
                                </li>
                                <li role="presentation">
                                    <a href="#tab" aria-controls="tab" role="tab" data-toggle="tab">Comuna</a>
                                </li>
                            </ul>
                        
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="home">
                                    <form action="/schools" method="POST" role="form">
                                        {{ csrf_field() }}                                               
                                        <div class="form-group">
                                            <input type="text" class="form-control" required id="q" name="q" placeholder="Ingresa el nombre o rbd">
                                        </div>         
                                        <button type="submit" id="btn-school" class="btn btn-primary btn-sm"><i class="fa fa-btn fa-search"></i> Buscar</button>
                                    </form>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="tab">
                                    <form action="/schools/commune" method="POST" role="form">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <input type="text" class="form-control" required id="commune" name="commune" placeholder="Ingresa una ciudad o comuna">
                                        </div>         
                                        <button type="submit" id="btn-commune" class="btn btn-primary btn-sm"><i class="fa fa-btn fa-search"></i> Buscar</button>
                                    </form>
                                </div>
                            </div>
                        </div><hr>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal search-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
        <script>
            $( document ).ready(function() {

                var options_q = {
                    url: function(q) {
                        return "/schools/" + q;
                    },
                    getValue: "name",
                    template: {
                        type: "description",
                        fields: {
                            description: "rbd"
                        }
                    },
                    list: {
                      onSelectItemEvent: function() {
                        var value = $("#q").getSelectedItemData().rbd;

                        $('#btn-school').on('click', function(event) {
                          event.preventDefault();
                          window.location.href = '/schools/rbd/'+ value;
                        });

                        $('input#q').on('keypress', function(event) {
                          event.preventDefault();
                          if(event.which === 13)
                            window.location.href = '/schools/rbd/'+ value;
                        });

                      }
                    }
                };
                var options_commune = {
                    url: function(commune) {
                        return "/commune/autocomplete/" + commune;
                    },
                    getValue: "commune_name",

                    list: {
                      onSelectItemEvent: function() {
                        var value = $("#commune").getSelectedItemData().commune_name.replace(' ', '-').toLowerCase();

                        $('#btn-commune').on('click', function(event) {
                          event.preventDefault();
                          window.location.href = '/commune/'+ value;
                        });

                        $('input#commune').on('keypress', function(event) {
                          event.preventDefault();
                          if(event.which === 13)
                            window.location.href = '/commune/'+ value;
                        });

                      }
                    }
                };
                $("#q").easyAutocomplete(options_q).focusout(function() {
                    this.value = "";
                });
                $("#commune").easyAutocomplete(options_commune).focusout(function() {
                    this.value = "";
                });
                // makes sure the whole site is loaded
                jQuery(window).load(function() {
                        // will first fade out the loading animation
                    jQuery("#status").delay(500).fadeOut();
                        // will fade out the whole DIV that covers the website.
                    jQuery("#preloader").delay(500).fadeOut("slow");
                });                
            });
        </script>
        <script src="https://unpkg.com/leaflet@1.2.0/dist/leaflet.js"></script>
        @stack('js')
        @stack('maps')
        @stack('graph')
</body>

</html>