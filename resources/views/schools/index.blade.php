@extends ('app')

@section('content')
	<div class="row">
		<div class="col-lg-3">
			<h4><i class="fa fa-filter fa-btn"></i> Filtros {{$level}}
			</h4><hr>          
	        <form method="GET">
		        <div class="form-group">
		            <label for="tipo">Nivel:</label>
		            <select name="nivel" id="tipo" class="form-control">
		                <option value="">Elija una opción</option>
		                <option {{ $level === '2m' || $level === NULL  ? 'selected' : '' }} value="2m">2° Medio</option>
		                <option {{ $level === '6b' ? 'selected' : '' }} value="6b">6° Básico</option>
		                <option {{ $level === '4b' ? 'selected' : '' }} value="4b">4° Básico</option>
		            </select>
		            <label for="agno">Año:</label>
	                <select name="agno" id="agno" class="form-control">
	                    <option value="">Elija una opción</option>
	                    <option value="2016">2016</option>
	                    <option value="2015">2015</option>
	                    <option value="2014">2014</option>
	                    <option value="2013">2013</option>
	                </select>
		        </div>
		        <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-btn fa-filter"></i> Filtrar</button>
	        </form>
	        <hr>
                <a class="btn btn-primary btn-xs" data-toggle="modal" href='#modal-search'>
                    <i class="fa fa-search fa-btn"></i>
                    Cambiar de colegio o comuna
                </a>
            <hr>
		</div>
		
		<div class="col-lg-9">
			<h4><i class="fa fa-btn fa-university"></i>
			<span>"{{ $schools->name }}"</span>
			<small>
				- <i class="fa fa-btn fa-map-marker"></i> Comuna: 
				<a href="/commune/{{ $schools->commune_name }}">{{ $schools->commune_name }}</a>
			</small>
			<small> 
				- {{ $schools->level === '6b' ? '6to Básico' : ($schools->level === '2m' ? '2do Medio' : '4to Básico') }}
			</small>
			<span>
				<a class="btn btn-primary btn-xs visible-lg visible-md pull-right" data-toggle="modal" href='#modal-map'>
				<i class="fa fa-btn fa-map-marker"></i>
				Ver ubicación
				</a>
			</span>
			</h4>
			<span>
				<a class="btn btn-primary btn-xs visible-xs visible-sm" data-toggle="modal" href='#modal-map'>
				<i class="fa fa-btn fa-map-marker"></i>
				Ver ubicación
				</a>
			</span>
				
			<hr>
			<div class="table-responsive well">
				<table id="schoolTable" class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>RBD</th>
							<th>Nombre</th>
							<th>Autoestima</th>
							<th>Ambiente</th>
							<th>Participación</th>
							<th>Hábitos</th>
						</tr>
					</thead>
					<tbody id="table-filter">					
							<tr>
								<td id="rbd">{{ $schools->rbd }}</td>
								<td id="name">{{ $schools->name }}</td>
								<td id="self_steem">{{ $schools->self_steem }}</td>
								<td id="environment">{{ $schools->environment }}</td>
								<td id="participation">{{ $schools->participation }}</td>
								<td id="habits">{{ $schools->habits }}</td>
								<td class="hidden" id="latitud">{{ $schools->latitud }}</td>
								<td class="hidden" id="longitud">{{ $schools->longitud }}</td>
							</tr>
							<table class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>Puntajes Simce:</th>
										<th>Matemáticas <span id="math" class="data label label-info">{{ $schools->math_pts }}</span></th>
										<th>Lenguaje <span id="language" class="data label label-warning">{{ $schools->language_pts }}</span></th>
										<th>Ciencias <span id="nature" class="data label label-success">{{ $schools->nature_pts }}</span></th>
									</tr>
									<tr>
										<th class=" community_ranking ">
											Ranking comunal: {{ $schools->community_ranking }}<i class="text-warning fa fa-star fa-btn-left"></i>
										</th>
										<th class="community_ranking ">
											Ranking nacional: {{ $schools->national_rank }}<i class="text-warning fa fa-star fa-btn-left"></i>
										</th>
									</tr>
								</thead>
							</table>					
					</tbody>			
				</table>

			</div>

			<div class="col-lg-12">
				<div class="row">
				<hr><h4>
						<i class="fa fa-btn fa-line-chart"></i>
						Mejor rankeados en <a href="/commune/{{ $schools->commune_name }}">{{ $schools->commune_name }}</a> en el mismo nivel.
					</h4><hr>		

					<?php 
						$math_related = array();
						$language_related = array();
						$nature_related = array();
						$school_name = array();
						$self_steem_related = array();
						$environment_related = array();
						$participation_related = array();
						$habits_related = array();  
					?>

					@foreach($related as $item)				
				        <div class="col-xs-12 col-md-4">
				            <div class="well well-sm">
				                <div class="row">
				                    <div class="col-xs-12 col-md-8">
										<h5 class="">
											<a href="/schools/rbd/{{ $item->rbd }}">{{ $item->name }}</a>
										</h5>
										<p><i>{{ $item->commune_name }}</i></p>
				                        <!-- end row -->
				                    </div>
				                  	<div class="col-xs-12 col-md-4 text-center">
				                        <h5 class="rating-num">
				                            {{ $item->community_ranking }} <span class=" fa fa-star fa-3 text-warning"></span></h5>
				                            <span>
												<i><strong>rbd</strong><br> {{ $schools->rbd }}</i>
				                            </span>
				                    </div>
				                    <div class="col-xs-12 text-center"><hr class="card-hr">
										<span>
											<i class="label label-info">M: 
												<span id="math_related">{{ $item->math_pts }}</span>
											</i>
										</span>
										<span>
											<i class="label label-warning">L: 
												<span id="language_related">{{ $item->language_pts }}</span>
											</i>
										</span>
										<span>
											<i class="label label-success">C: 
												<span id="nature_related">{{ $item->nature_pts }}</span>
											</i>
										</span>
										<?php 
											$math_related[] = $item->math_pts ; 
											$language_related[] = $item->language_pts ;
											$nature_related[] = $item->nature_pts ;
											$school_name[] = $item->name ;

											$self_steem_related[] = $item->self_steem ; 
											$environment_related[] = $item->environment ;
											$participation_related[] = $item->participation ;
											$habits_related[] = $item->habits ;
										?>
				                    </div>
				                </div>
				            </div>
				        </div>
				    @endforeach
			    </div>
			</div>

			<div class="modal fade" id="modal-map">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h5>{{ $schools->name }}</h5>
						</div>
						<div class="modal-body">
							<div id="map_canvas" style="height: 400px"></div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</button>
						</div>
					</div>
				</div>
			</div>			
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12"><hr>
			<h4><i class="fa fa-bar-chart fa-btn"></i> Gráficos comparativos</h4><hr>
			<div class="col-lg-6">
				<div class="row">
					<canvas id="bar-chart" height="200%"></canvas>	
				</div> 				
			</div>
			<div class="col-lg-6">
				<div class="row">
					<canvas id="bar-chart-2" height="200%"></canvas>
				</div> 				
			</div>
		</div>
	</div>



	
@endsection

@push('graph')

	<script>
		var math_related = <?php echo json_encode($math_related); ?>;
		var language_related = <?php echo json_encode($language_related); ?>;
		var nature_related = <?php echo json_encode($nature_related); ?>;
		var school_name = <?php echo json_encode($school_name); ?>;

		var self_steem_related = <?php echo json_encode($self_steem_related); ?>;
		var environment_related = <?php echo json_encode($environment_related); ?>;
		var participation_related = <?php echo json_encode($participation_related); ?>;
		var habits_related = <?php echo json_encode($habits_related); ?>;

		var name = $("#name").html();
		var self_steem = $("#self_steem").html();
    	var environment = $("#environment").html();
    	var participation = $("#participation").html();
    	var habits = $("#habits").html();
    	var math = $("#math").html();
    	var language = $("#language").html();
    	var nature = $("#nature").html();

		var ctx = document.getElementById('bar-chart').getContext('2d');
		var chart = new Chart(ctx, {
		    // The type of chart we want to create
		    type: 'bar',

		    // The data for our dataset
		    data: {
		        labels: ["Autoestima", "Ambiente", "Participacion", "Hábitos"],
		        datasets: [{
		            label: name,
		            backgroundColor: '#3498db',
		            borderColor: '#3498db',
		            data: [parseFloat(self_steem), parseFloat(environment), parseFloat(participation), parseFloat(habits)],
		        },
		        {
		            label: school_name[0],
		            backgroundColor: '#10C1CF',
		            borderColor: '#10C1CF',
		            data: [self_steem_related[0], environment_related[0], participation_related[0], habits_related[0]],
		        },
		        {
		            label: school_name[1],
		            backgroundColor: 'rgba(255, 159, 64, 0.5)',
		            borderColor: 'rgba(255, 159, 64, 0.5)',
		            data: [self_steem_related[1], environment_related[1], participation_related[1], habits_related[1]],
		        },
		        {
		            label: school_name[2],
		            backgroundColor: 'rgba(75, 192, 192, 0.5)',
		            borderColor: 'rgba(75, 192, 192, 0.5)',
		            data: [self_steem_related[2], environment_related[2], participation_related[2], habits_related[2]],
		        }]
		    },

		    // Configuration options go here
		    options: {
		    	scales: {
			        yAxes: [{
			            ticks: {
			                beginAtZero:true
			            },
			            scaleLabel: {
				            display: true,
				            labelString: 'Nivel del indicador'
				         }
			        }],
			        xAxes: [{
					    scaleLabel: {
					        display: true,
					        labelString: 'Indicador'
					    }
				    }]
			    },
			    title: {
		            display: true,
		            text: 'Indicadores'
		        }
		    }
		});
		var ctx2 = document.getElementById('bar-chart-2').getContext('2d');
		var chart = new Chart(ctx2, {
		    // The type of chart we want to create
		    type: 'bar',

		    // The data for our dataset
		    data: {
		        labels: ["Matemáticas", "Lenguaje", "Ciencias"],
		        datasets: [{
		            label: name,
		            backgroundColor: '#3498db',
		            borderColor: '#3498db',
		            data: [parseFloat(math), parseFloat(language), parseFloat(nature)],
		        },

		        {
		            label: school_name[0],
		            backgroundColor: '#10C1CF',
		            borderColor: '#10C1CF',
		            data: [math_related[0], language_related[0], nature_related[0]],
		        },
		        {
		            label: school_name[1],
		            backgroundColor: 'rgba(255, 159, 64, 0.5)',
		            borderColor: 'rgba(255, 159, 64, 0.5)',
		            data: [math_related[1], language_related[1], nature_related[1]],
		        },
		        {
		            label: school_name[2],
		            backgroundColor: 'rgba(75, 192, 192, 0.5)',
		            borderColor: 'rgba(75, 192, 192, 0.8)',
		            data: [math_related[2], language_related[2], nature_related[2]],
		        }]
		    },

		    // Configuration options go here
		    options: {
		    	scales: {
			        yAxes: [{
			            ticks: {
			                beginAtZero:true
			            },
			            scaleLabel: {
				            display: true,
				            labelString: 'Puntaje'
				        }
			        }],
			        xAxes: [{
					    scaleLabel: {
					        display: true,
					        labelString: 'Materias'
					    }
				    }]
			    },
			    title: {
		            display: true,
		            text: 'Notas SIMCE'
		        }
		    }
		});
	</script>
@endpush

@push('maps')

	<script>
		var latitud = $("#latitud").html();
    	var longitud = $("#longitud").html();
    	var name = $("#name").html();
    	var rbd = $("#rbd").html();

    	var latitud = parseFloat(latitud.replace(",", "."));
    	var longitud = parseFloat(longitud.replace(",", "."));

    	//calcular distancia entre 2 colegios (falta)
    	function round(dist) {
		  return +(Math.round(dist + "e+2")  + "e-2");
		}
	 	function getDistance(lat1, lon1, lat2, lon2) {
			var radlat1 = Math.PI * lat1/180
			var radlat2 = Math.PI * lat2/180
			var theta = lon1-lon2
			var radtheta = Math.PI * theta/180
			var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
			dist = Math.acos(dist)
			dist = dist * 180/Math.PI
			dist = dist * 60 * 1.1515
			dist = dist * 1.609344 //distancia en kilometros
			return round(dist)
		}

		var distance = getDistance(latitud, longitud,-35.4419820, -71.684370)

		console.log(distance)

	    function initMap() {

	        var mymap = L.map('map_canvas').setView([latitud, longitud], 17);
	        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
		        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
		    }).addTo(mymap);
	        var marker = L.marker([latitud, longitud]).addTo(mymap);
	        marker.bindPopup("<b>"+name+"</b><br>RBD: "+rbd+"").openPopup();
	        
	    }
	    //iniciar mapa en modal
	    $('#modal-map').on('shown.bs.modal', function () { 
		    initMap();
		});

    </script>    

@endpushsh