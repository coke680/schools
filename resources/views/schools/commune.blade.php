@extends ('app')

@section('content')

		@foreach ($commune[0] as $name)
			<h4>Colegios de {{ $name }}</h4>
		@endforeach
		<hr>

		@foreach (array_chunk($schools, 3) as $items)
			<div class="row">
				@foreach($items as $school)
			        <div class="col-xs-12 col-md-4">
			            <div class="well well-sm">
			                <div class="row">
			                    <div class="col-xs-12 col-md-8">
									<h5 class=""><a href="/schools/{{ $school->rbd }}">{{ $school->name }}</a></h5>
			                        <!-- end row -->
			                    </div>
			                  	<div class="col-xs-12 col-md-4 text-center">
			                        <h5 class="rating-num">
			                            4.0 <span class=" fa fa-star fa-3 text-warning"></span></h5>
			                            <span><i>rbd: {{ $school->rbd }}</i></span>
			                    </div>
			                    <div class="col-xs-12 text-center"><hr class="card-hr">
									<span><i class="label label-info">M: {{ $school->math_pts }}</i></span>
									<span><i class="label label-warning">L: {{ $school->language_pts }}</i></span>
									<span><i class="label label-success">C: {{ $school->nature_pts }}</i></span>
			                    </div>
			                </div>
			            </div>
			        </div>
			    @endforeach
		    </div>
		@endforeach
	
@endsection
