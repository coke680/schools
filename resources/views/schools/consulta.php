        if($request->has('commune')) {

            $schools = School::where('level', '2m')
                ->where('commune_name', $request->commune)
                ->orWhere('rbd', $request->commune)
                ->orderBy('community_ranking')
                ->get();

            $commune = DB::select('select distinct commune_name from school where commune_name = \''.$request->commune.'\'');

            return view('commune.index', compact('schools','commune')); 
        }
        else {

            return view('/welcome');
        }        
        