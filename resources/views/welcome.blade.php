<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Colegios</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- Latest compiled and minified CSS & JS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <script src="//code.jquery.com/jquery.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://bootswatch.com/flatly/bootstrap.css">
    
        <!-- JS file -->
        <script src="js/jquery.easy-autocomplete.js"></script>
        <!-- CSS file -->
        <link rel="stylesheet" href="css/easy-autocomplete.css">
        <link rel="stylesheet" href="css/custom.css">
        <!-- Styles -->
        <style>
            html, body {
                background: url('http://wlogmop.nl/wp-content/uploads/2016/03/Naamloos-1.png');
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 200;
                margin: 0;
            }

            .full-height {
                height: 80vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .foot-links a, .foot-links p{
              display: block;
               color: rgb(63, 63, 63);
              transition: all 0.2s ease-in-out;
              -webkit-transition: all 0.2s ease-in-out;
              text-decoration: none;
              font-size: 13px;
              color: #f4f4f4;
              padding-bottom: 5px;
              
            }
            .foot-links img{
                padding-top: 5px;
            }
            #footer{
              background: #0d0d4d;
              margin-top: 280px;
              padding-top: 40px;
              padding-bottom: 30px;
             }
            .foot-header{
              font-size: 18px;
              color: #f4f4f4;
              padding-bottom: 20px;
            }
              
            #footer .container{
                max-width: 1100px;
            }
            #bottom-footer{
              margin-bottom: 20px;
                border-top: 1px solid #ccc;
                border-bottom: 1px solid #ccc;
                padding-top: 15px;
                padding-bottom: 15px;
            }
            #bottom-footer a{
                  text-decoration: none;
              color: #626262;
              -webkit-transition: all .2s ease-in-out;
              -moz-transition: all .2s ease-in-out;
              -o-transition: all .2s ease-in-out;
              -ms-transition: all .2s ease-in-out;
              transition: all .2s ease-in-out;
              font-size: 12px;
              padding: 0px 15px;
              border-right: 1px solid #ccc;
              font-size: 13px;
              color: #626262;
              padding: 0 12px;
            }
            #final-footer{
                font-size: 11px;
                color: #666;
            }
            #final-footer i{
                font-size: auto;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div id="info"></div>
                <h2 class="m-b-md">Algún título de la aplicación<br>para dar a conocer la app</h2>
                <h4>Busca tu colegio</h4><hr> 
                <div role="tabpanel" class="col-lg-12"> 
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#home" aria-controls="home" role="tab" data-toggle="tab">Nombre o RBD</a>
                        </li>
                        <li role="presentation">
                            <a href="#tab" aria-controls="tab" role="tab" data-toggle="tab">Ciudad o comuna</a>
                        </li>
                    </ul>
                
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">
                            <form action="/schools" method="POST" role="form">
                                {{ csrf_field() }}                                               
                                <div class="form-group">
                                    <input type="text" class="form-control" required id="q" autofocus name="q" placeholder="Ingresa el nombre o rbd">
                                </div>
                                <button type="submit" id="btn-school" class="btn btn-primary btn-sm"><i class="fa fa-btn fa-search"></i> Buscar</button>
                            </form>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="tab">
                            <form action="/schools/commune" method="POST" role="form">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <input type="text" class="form-control" required id="commune" name="commune" placeholder="Ingresa una ciudad o comuna">
                                </div>         
                                <button type="submit" id="btn-commune" class="btn btn-primary btn-sm"><i class="fa fa-btn fa-search"></i> Buscar</button>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div id="footer">
            <div class="container">
              <div class="row text-center">
                
                  <div class="col-sm-3">
                    <div class="foot-header">
                      Acerca de Nosotros <img src="http://200.27.156.170/ean_default/img/cocha/icon-cocha.png">
                    </div>
                    <div class="foot-links">
                      <a href="http://internet.cocha.com/nuestra-empresa/nuestra-empresa.html">Nuestra Empresa</a>
                      <a href="http://cms.cocha.com/sucursales.html">Sucursales</a>
                      <a href="http://internet.cocha.com/_DisenoWeb/minisitios/sitio/por-que-comprar-en-cocha.html?cid=por-que-comprar-en-cocha">Por qué comprar en Cocha</a>
                      <a href="http://internet.cocha.com/virgin_galactic/virgin_galactic.html">Virgin Galactic</a>
                      <a href="http://internet.cocha.com/_DisenoWeb/minisitios/sitio/trabaja-con-nosotros.html?cid=trabaja_en_cocha">Trabaja con nosotros</a>
                      <a href="http://www.cochainbound.com/" target="_blank">¿Vienes a Chile?</a>
                    </div>
                  </div><!--/col-sm-3-->
                <div class="col-sm-3">
                  <div class="foot-header"> Servicio al cliente <img src="http://200.27.156.170/ean_default/img/cocha/servicio-al-cliente-icon.png"></div>
                  <div class="foot-links">
                    <a href="javascript:Contacto()">Contáctanos</a>
                    <a href="http://internet.cocha.com/_DisenoWeb/minisitios/sitio/servicio-al-pasajero.html?cid=atencion-al-pasajero">Atención al pasajero</a>
                    <a href="http://internet.cocha.com/_DisenoWeb/minisitios/sitio/faq.html?cid=preguntas-frecuentes">Preguntas frecuentes</a>
                    <a href="http://cms.cocha.com/terminos-y-condiciones">Términos y Condiciones Generales</a>
                    <a href="http://internet.cocha.com/_DisenoWeb/check-in/check-in.html?cid=check-in-on-line">Check-in en línea </a>
                    <a href="http://www.cocha.com/ibe/bookingManagement/retrieveBookingForm.do">Consultar reserva</a>
                    <a href="http://internet.cocha.com/manual-del-viajero.pdf" target="_blank">Manual del viajero</a>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="foot-header">
                    Medios de pago <img src="http://200.27.156.170/ean_default/img/cocha/card-icon.png">
                  </div>
                  <div class="foot-links">
                    <p>
                      <i class="fa fa-check text-success"></i> Tarjetas de crédito<br>
                      <img src="http://200.27.156.170/ean_default/img/cocha/visa-card.png">
                      <img src="http://200.27.156.170/ean_default/img/cocha/master-card.png">
                      <img src="http://200.27.156.170/ean_default/img/cocha/dinner-club-card.png">
                    </p>
                    <p>
                      <i class="fa fa-check text-success"></i> Transferencias bancarias<br>
                      <img src="http://200.27.156.170/ean_default/img/cocha/banco-santander-card-1.png">
                      <img src="http://200.27.156.170/ean_default/img/cocha/banco-de-chile-card-1.png">
                    </p>
                    <p>
                      <i class="fa fa-check text-success"></i> Tarjeta Ripley<br>
                      <img src="http://200.27.156.170/ean_default/img/cocha/ripley-card.png">
                    </p>
                  </div>
                </div><!--/col-sm-3-->
                <div class="col-sm-3">
                  <img src="http://200.27.156.170/ean_default/img/cocha/tripadvisor-logo.jpg" class="img-responsive img-thumbnail">
                  <br><br>
                  <a href="http://internet.cocha.com/especiales/sello-de-calidad-turistica.html" style="float: center;">
                    <img src="http://200.27.156.170/ean_default/img/cocha/sello_de_calidad_turistica.png" width="121" height="70" alt="sello de calidad turistica">
                  </a>
                  <a href="http://www.chileestuyo.cl/" target="_blanck" style="float: left; margin-left: 20px;">
                    <img src="http://200.27.156.170/ean_default/img/cocha/Logo_Chileestuyo.png" height="70" alt="Logo Chileestuyo">
                  </a>
                </div>                
            </div><!--/row-->
        </div><!--/footer--></div>
        <script>
            $( document ).ready(function() {

                var options_q = {
                    url: function(q) {
                        return "schools/" + q;
                    },

                    getValue: "name",

                    template: {
                        type: "description",
                        fields: {
                            description: "rbd"
                        }
                    },
                    list: {
                      onSelectItemEvent: function() {
                        var value = $("#q").getSelectedItemData().rbd;

                        $('#btn-school').on('click', function(event) {
                          event.preventDefault();
                          window.location.href = '/schools/rbd/'+ value;
                        });

                        $('input#q').on('keypress', function(event) {
                          event.preventDefault();
                          if(event.which === 13)
                            window.location.href = '/schools/rbd/'+ value;
                        });

                      }
                    }
                };

                var options_commune = {
                    url: function(commune) {
                        return "commune/autocomplete/" + commune;
                    },

                    getValue: "commune_name",
                    list: {
                      onSelectItemEvent: function() {
                        var value = $("#commune").getSelectedItemData().commune_name.replace(' ', '-').toLowerCase();

                        $('#btn-commune').on('click', function(event) {
                          event.preventDefault();
                          window.location.href = '/commune/'+ value;
                        });

                        $('input#commune').on('keypress', function(event) {
                          event.preventDefault();
                          if(event.which === 13)
                            window.location.href = '/commune/'+ value;
                        });

                      }
                    }

                };
                $("#q").easyAutocomplete(options_q).focusout(function() {
                    this.value = "";
                });
                $("#commune").easyAutocomplete(options_commune).focusout(function() {
                    this.value = "";
                });
                
            });
        </script>
    </body>
</html>
