@extends ('app')

@section('content')

	<div class="row">
		<div class="col-lg-3">
			<h4><i class="fa fa-filter fa-btn"></i> Filtros</h4><hr>          
            <form id="form-filter">
	            <div class="form-group">
	                <label for="nivel">Nivel:</label>
	                <select name="nivel" id="nivel" class="form-control">
	                    <option value="">Elija una opción</option>
	                    <option value="2m" selected>2° Medio</option>
	                    <option value="6b">6° Básico</option>
	                    <option value="4b">4° Básico</option>
	                </select>
	                <label for="agno">Año:</label>
	                <select name="agno" id="agno" class="form-control">
	                    <option value="">Elija una opción</option>
	                    <option value="2016">2016</option>
	                    <option value="2015">2015</option>
	                    <option value="2014">2014</option>
	                    <option value="2013">2013</option>
	                </select>
	                <label for="tipo">Dependencia:</label>
	                <select name="tipo" id="tipo" class="form-control">
	                    <option value="">Elija una opción</option>
	                    <option value="municipal">Municipal</option>
	                    <option value="particular pagado">Particular</option>
	                    <option value="particular subvencionado">Subvencionado</option>
	                </select>
	                <label for="religion">Religión:</label>
	                <select name="religion" id="religion" class="form-control">
	                    <option value="">Elija una opción</option>
	                    <option value="catolico">Católico</option>
	                    <option value="laico">Laico</option>
	                    <option value="evangelico">Evangélico</option>
	                    <option value="otro">Otro</option>
	                </select>
	            </div>

                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-btn fa-filter"></i> Filtrar</button>
            </form>
            <hr>
                <a class="btn btn-primary btn-xs" data-toggle="modal" href='#modal-search'>
                    <i class="fa fa-search fa-btn"></i>
                    Cambiar de colegio o comuna
                </a>
            <hr>
		</div>			

		<div class="col-lg-9">

			<h4><i class="fa fa-btn fa-university"></i> Colegios de <span class="text-capitalize">{{ $commune }}</span> ordenados por ranking comunal</h4>
			<hr>

			<div id="content">
			@foreach ($schools->chunk(3) as $items)
				<div class="row">
					@foreach($items as $school)
				        <div class="col-xs-12 col-md-4">
				            <div class="well well-sm">
				                <div class="row">
				                    <div class="col-xs-12 col-md-8">
										<h5 class="">
											<a href="/schools/rbd/{{ $school->rbd }}?level={{ $school->level }}">{{ $school->name }}</a>
										</h5>
										<p><i>{{ $school->commune_name }}</i></p>
				                        <!-- end row -->
				                    </div>
				                  	<div class="col-xs-12 col-md-4 text-center">
				                        <h5 class="rating-num">
				                            {{ $school->community_ranking }} <span class=" fa fa-star fa-3 text-warning"></span></h5>
				                            <span>
				                            	<i><strong>rbd</strong><br> {{ $school->rbd }}</i>
				                            </span>
				                    </div>
				                    <div class="col-xs-12 text-center"><hr class="card-hr">
										<span><i class="label label-info">M: {{ $school->math_pts }}</i></span>
										<span><i class="label label-warning">L: {{ $school->language_pts }}</i></span>
										<span><i class="label label-success">C: {{ $school->nature_pts }}</i></span>
				                    </div>
				                </div>
				            </div>
				        </div>
				    @endforeach
			    </div>
			@endforeach
			</div>
		</div>
	</div>

	<!-- Modal dialog-->
	<div class="modal fade" id="pleaseWaitDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	          <h2 class="text-center">Consultando...</h2>
	      </div>
	      <div class="modal-body">
	        <div class="progress">
	          <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>

@endsection

@push('js')

<script type="text/javascript">
	$(document).ready(function() {

	    $('#form-filter').on('submit', function(event) { //change para cambiar filtro sin sumbit
	        event.preventDefault();
	        $('#pleaseWaitDialog').modal();
	        $.ajax({
	            url: '{{ route('commune', [$commune]) }}',
	            type: 'GET',
	            dataType: 'JSON',
	            data: $(this).serialize(),
	        })
	        .done(function(response) {

	            $('#pleaseWaitDialog').modal('hide');

	            var content = $('#content');

	            content.empty();

	            var html = '';

	            $.each(response, function(index, data) {
	                html += `<div class="row">`;
						$.each(data, function(key, value) {
	                        html += `<div class="col-xs-12 col-md-4">
					        <div class="well well-sm">
					            <div class="row">
					                <div class="col-xs-12 col-md-8">
					                        <h5 class="">
					                            <a href="/schools/rbd/${value.rbd}?nivel=${value.level}">${value.name}</a>
					                        </h5>
					                        <p><i>${value.commune_name}</i></p>
					                        <!-- end row -->
					                    </div>
					                    <div class="col-xs-12 col-md-4 text-center">
					                        <h5 class="rating-num">
					                            ${value.community_ranking}
					                             <span class=" fa fa-star fa-3 text-warning"></span></h5>
					                            <span>
					                                <i><strong>rbd</strong><br> ${value.rbd}</i>
					                            </span>
					                    </div>
					                    <div class="col-xs-12 text-center"><hr class="card-hr">
					                        <span><i class="label label-info">M: ${value.math_pts}</i></span>
					                        <span><i class="label label-warning">L: ${value.language_pts}</i></span>
					                        <span><i class="label label-success">C: ${value.nature_pts}</i></span>
					                    </div>
					                </div>
					            </div>
					        </div>`;
	                        });
	                	html += `</div>`;
	            });

	            content.append(html);
	        })
	        .fail(function() {
	            console.log("error");
	        })
	        .always(function() {
	            console.log("complete");
	        });
	        
	    });		
	});
</script>

@endpush