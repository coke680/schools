<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\School;
use Auth;
use DB;
use Illuminate\Http\Request;
use Session;

class SchoolsController extends Controller {


    public function index(Request $request) {

    	if($request->has('q')) {

            $schools = DB::select('select * from school where level = "2m" and name = \''.$request->q.'\' OR rbd = \''.$request->q.'\'');

            $related = DB::select('select * from school where commune_name = \''.$schools[0]->commune_name.'\' and level = \''.$schools[0]->level.'\' order by community_ranking limit 3');

            return view('schools.index', compact('schools','related')); 
        }
        else {

        	return view('/welcome');
        }       
        
    }

    public function getSchool(Request $request) {

        $schools = School::where('rbd', $request->rbd)
                    ->when($request->nivel, function ($query) use ($request) {
                        return $query->where('level', $request->nivel);
                    })->firstOrFail();

        $related = School::where('commune_name', $schools->commune_name)
                            ->where('level', $schools->level)
                            ->orderBy('community_ranking')
                            ->limit(3)
                            ->get();

        $level = $request->nivel ?: null;

        return view('schools.index', compact('schools','related', 'level'));
    }

    public function autocomplete(Request $request) { 

        $schools = DB::select('select distinct rbd, name from school where name LIKE \'%'.$request->q.'%\' OR rbd LIKE \'%'.$request->q.'%\' limit 5');
        return $schools;        
    }

}
