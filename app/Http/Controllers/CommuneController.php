<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Auth;
use Session;
use App\School;

class CommuneController extends Controller
{
    public function index($commune, Request $request) {

            $commune = str_replace('-', ' ', $commune);

            $schools = School::where('commune_name', $commune)
                    ->orderBy('community_ranking');

            if($request->ajax())
            {
                return $schools = $schools->when($request->nivel, function ($query) use ($request) {
                    return $query->where('level', $request->nivel);
                })->when($request->tipo, function ($query) use ($request) {
                    return $query->where('dependence', $request->tipo);
                })->when($request->religion, function ($query) use ($request) {
                    return $query->where('religion', $request->religion);
                })->get()->chunk(3);
            }
            
            $schools = $schools->where('level', '2m')->get();

            return view('commune.index', compact('schools','commune'));   
        
    }
    public function autocomplete_commune(Request $request) { 

        $commune = DB::select('select distinct commune_name from school where commune_name LIKE \'%'.$request->commune.'%\' limit 5');
        return $commune;        
    }
}
